package com.whiterabbit.wheresmydriver;


import android.content.Intent;
import android.location.Location;
import android.support.test.rule.ActivityTestRule;

import com.jakewharton.rxrelay.BehaviorRelay;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.whiterabbit.wheresmydriver.inject.MockModule;
import com.whiterabbit.wheresmydriver.inject.TestAppComponentRule;
import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;
import com.whiterabbit.wheresmydriver.screens.main.DriversActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import mockdata.EspressoTestSchedulersProvider;
import mockdata.TestDataProvider;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;

import static android.support.test.InstrumentationRegistry.getTargetContext;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsNot.not;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class MainScreenTest {

    @Rule
    public final ActivityTestRule<DriversActivity> activity = new ActivityTestRule<>(DriversActivity.class, false, false);

    @Rule
    public final TestAppComponentRule componentRule = new TestAppComponentRule();

    private DriversRestClient mockClient;
    private RxPermissions mockPermission;
    private ReactiveLocationProvider mockLocation;
    private SchedulersProvider fakeScheduler;

    private TestDataProvider dataProvider;

    @Before
    public void setup() throws Throwable {
        dataProvider = new TestDataProvider();
        MockModule mockAppModule = componentRule.getMockAppModule();

        mockClient = mock(DriversRestClient.class);
        mockPermission = mock(RxPermissions.class);
        mockLocation = mock(ReactiveLocationProvider.class);
        fakeScheduler = new EspressoTestSchedulersProvider();
        when(mockPermission.request(any(), any())).thenReturn(Observable.just(true));
        mockAppModule.setMockLocationProvider(mockLocation);
        mockAppModule.setMockPermissionProvider(mockPermission);
        mockAppModule.setMockRestClient(mockClient);
        mockAppModule.setScheduler(fakeScheduler);
    }

    private void startActivity() {
        Intent intent = new Intent(getTargetContext(), DriversActivity.class);
        activity.launchActivity(intent);
    }

    @Test
    public void testDisplayNearestDetail() throws Throwable {
        when(mockClient.getDrivers(any(Coordinates.class)))
                .thenReturn(Observable.just(dataProvider.getAllDrivers()));

        Location nearPisa = new Location("Near Pisa");
        nearPisa.setLatitude(43.725677);
        nearPisa.setLongitude(10.513911);

        Location nearFirenze = new Location("Near Firenze");
        nearFirenze.setLatitude(43.876369);
        nearFirenze.setLongitude(11.150270);

        BehaviorRelay<Location> fakeLocationProvider = BehaviorRelay.create();

        when(mockLocation.getUpdatedLocation(any()))
                .thenReturn(fakeLocationProvider);

        startActivity();

        onView(withId(R.id.driver_info)).check(matches(not(isDisplayed())));
        onView(withId(R.id.center_driver_button)).check(matches(not(isDisplayed())));

        fakeLocationProvider.call(nearPisa);
        onView(withId(R.id.driver_info)).check(matches(isDisplayed()));
        onView(withId(R.id.center_driver_button)).check(matches(isDisplayed()));
        onView(withId(R.id.driver_info)).check(matches(withText("Pisa Delapique")));
        onView(withId(R.id.driver_distance)).check(matches(withText("9 km")));

        fakeLocationProvider.call(nearFirenze);

        onView(withId(R.id.driver_info)).check(matches(withText("Firenze Fanastick")));
        onView(withId(R.id.driver_distance)).check(matches(withText("14 km")));

    }

}