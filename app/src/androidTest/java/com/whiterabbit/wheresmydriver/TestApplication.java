package com.whiterabbit.wheresmydriver;


import com.whiterabbit.wheresmydriver.inject.ApplicationModule;

public class TestApplication extends WMDApplication {
    private ApplicationModule mApplicationModule;

    @Override
    ApplicationModule getApplicationModule() {
        if (mApplicationModule == null)
            return super.getApplicationModule();
        return mApplicationModule;
    }

    public void setApplicationModule(ApplicationModule m) {
        mApplicationModule = m;
    }

    public void reset() {
        mApplicationModule = null;
        initComponent();
    }
}
