package com.whiterabbit.wheresmydriver.inject;

import com.whiterabbit.wheresmydriver.TestApplication;

import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;

import static android.support.test.InstrumentationRegistry.getTargetContext;


public class TestAppComponentRule implements TestRule {
    private MockModule mockAppModule;

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override public void evaluate() throws Throwable {
                TestApplication app = (TestApplication) getTargetContext().getApplicationContext();
                try {
                    mockAppModule = new MockModule(app);
                    app.setApplicationModule(mockAppModule);
                    base.evaluate();
                } finally {
                    app.reset();

                }
            }
        };
    }

    public MockModule getMockAppModule() {
        return mockAppModule;
    }
}