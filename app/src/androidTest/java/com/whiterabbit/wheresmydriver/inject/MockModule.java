package com.whiterabbit.wheresmydriver.inject;


import android.app.Application;
import android.content.Context;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;

import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

public class MockModule extends ApplicationModule {
    private DriversRestClient mMockRestClient;
    private ReactiveLocationProvider mMockLocationProvider;
    private RxPermissions mMockPermissionProvider;
    private SchedulersProvider mScheduler;

    public MockModule(Application app) {
        super(app);
    }

    @Override
    ReactiveLocationProvider provideLocation() {
        if (mMockLocationProvider == null)
            return super.provideLocation();
        return mMockLocationProvider;
    }

    @Override
    RxPermissions provideRxPermissions(Context c) {
        if (mMockPermissionProvider == null) {
            return super.provideRxPermissions(c);
        }
        return mMockPermissionProvider;
    }

    @Override
    DriversRestClient provideRestClient() {
        if (mMockRestClient == null) {
            return super.provideRestClient();
        }
        return mMockRestClient;
    }

    @Override
    SchedulersProvider provideSchedulers() {
        if (mScheduler == null) {
            return super.provideSchedulers();
        }
        return mScheduler;
    }

    public void setMockRestClient(DriversRestClient mMockRestClient) {
        this.mMockRestClient = mMockRestClient;
    }

    public void setMockLocationProvider(ReactiveLocationProvider mMockLocationProvider) {
        this.mMockLocationProvider = mMockLocationProvider;
    }

    public void setMockPermissionProvider(RxPermissions mMockPermissionProvider) {
        this.mMockPermissionProvider = mMockPermissionProvider;
    }

    public void setScheduler(SchedulersProvider scheduler) {
        mScheduler = scheduler;
    }
}
