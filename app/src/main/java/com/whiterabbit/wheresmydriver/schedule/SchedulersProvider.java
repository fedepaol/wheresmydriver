package com.whiterabbit.wheresmydriver.schedule;

import rx.Scheduler;

public interface SchedulersProvider {
    Scheduler main();

    Scheduler io();

    Scheduler computation();
}
