package com.whiterabbit.wheresmydriver.screens.main;


import com.whiterabbit.wheresmydriver.R;
import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.model.Driver;
import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;
import com.whiterabbit.wheresmydriver.utils.Constants;
import com.whiterabbit.wheresmydriver.utils.DistanceCalculator;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.subscriptions.CompositeSubscription;

public class DriversPresenterImpl implements DriversPresenter {
    private static class MapResult {
        Coordinates location;
        List<Driver> drivers;
        Driver nearest;
    }

    private SchedulersProvider mSchedulers;
    private DistanceCalculator mCalculator;
    private DriversView mView;
    private CompositeSubscription mSubscription;
    private DriversRestClient mRestClient;
    private Coordinates mCurrentLocation;
    private Driver mNearestDriver;
    private List<Driver> mDrivers;
    private long mDistance = 0;


    public DriversPresenterImpl(SchedulersProvider schedulersProvider,
                                DriversRestClient restClient,
                                DistanceCalculator calculator) {

        mSchedulers = schedulersProvider;
        mRestClient = restClient;
        mCalculator = calculator;
    }

    private Driver findNearest(List<Driver> drivers, Coordinates location) {
        return Collections.min(drivers, (d, d1) -> {
            double dist1 = mCalculator.distance(d.getCoordinates(), location);
            double dist2 = mCalculator.distance(d1.getCoordinates(), location);

            if (dist1 > dist2) return 1;
            if (dist1 < dist2) return -1;
            return 0;
        });
    }

    private MapResult fillMapResult(List<Driver> drivers, Coordinates currentLocation) {
        MapResult res = new MapResult();
        res.location = currentLocation;
        res.nearest = findNearest(drivers, currentLocation);
        res.drivers = drivers;
        return res;
    }

    private void onDataUpdate(MapResult res) {
        mCurrentLocation = res.location;
        mDrivers = res.drivers;

        mView.updateDrivers(mDrivers, res.nearest);
        changeNearestDriver(res.nearest);
    }

    private void onLocationUpdated(Coordinates location) {
        if (mDrivers == null) {
            return;
        }
        mCurrentLocation = location;
        Driver nearest = findNearest(mDrivers, location);
        changeNearestDriver(nearest);
    }

    private void changeNearestDriver(Driver newNearest) {
        long distance = mCalculator.distance(newNearest.getCoordinates(),
                mCurrentLocation);
        boolean wasNull = (mNearestDriver == null);

        if (mNearestDriver == null ||
                !mNearestDriver.equals(newNearest) ||
                mDistance != distance) {

            mNearestDriver = newNearest;
            mDistance = distance;
            if (wasNull) {
                mView.displayNearestDriverInfo();
            }
            mView.updateNearestDriverInfo(mNearestDriver, distance);
        }
    }

    private void centerView(Coordinates coordinates) {
        mView.centerTo(coordinates);
    }

    private void startSubscriptions(boolean canAskLocation, CompositeSubscription subscription) {
        Observable<Coordinates> locationObservable;
        if (canAskLocation) {
            Observable<Coordinates> o = mView.getLocationUpdates();
            // if the first location is not emitted within 3 seconds, uses paris one and hope location
            // will come soon or later
            locationObservable = o.take(1).timeout(3, TimeUnit.SECONDS, mSchedulers.computation()) // wait the first element for 3 seconds
                    .concatWith(o.skip(1)) // concats the next elements with the first
                    .onErrorResumeNext(
                            Observable.just(Constants.PARIS_COORDINATES) // if the 3 seconds expire emits paris location
                                    .concatWith(o)); // concat with the location stream
        } else {
            locationObservable = Observable.just(Constants.PARIS_COORDINATES);
        }

        // The following observable takes into account the case where location provider is not pushing updates
        // CombineLatest with the interval plus the sample should achieve a polling of the drivers
        // every 5 seconds using the last emitted location

        Observable<Coordinates> sampledLocationObservable =
                Observable.merge(
                        locationObservable.take(1), // the first element is emitted immediately

                        Observable.combineLatest(locationObservable,    // combines the last emitted position
                                Observable.interval(2500, TimeUnit.MILLISECONDS, // with an element emitted every 2.5 seconds
                                        mSchedulers.computation()),
                                (loc, time) -> loc)     // the result is the last location emitted when new OR every 2.5 seconds
                                .sample(5, TimeUnit.SECONDS, // the last updated location is sampled every 5 seconds
                                        mSchedulers.computation()));

        // the location observable is then used to feed the rest client in order to retrieve the drivers
        // related to the last location
        subscription.add(sampledLocationObservable
                .observeOn(mSchedulers.io())
                .flatMap(c -> mRestClient.getDrivers(c).map(drivers -> fillMapResult(drivers, c)))
                .subscribeOn(mSchedulers.io())
                .observeOn(mSchedulers.main())
                .doOnError(e -> mView.notifyError(R.string.failed_to_get_drivers_informations))
                .retryWhen(e -> e.flatMap(error -> Observable.timer(5, TimeUnit.SECONDS, // in case of error waits for 5 seconds
                        mSchedulers.computation())))
                .subscribe(this::onDataUpdate));

        // The location update is more frequent and should be handled even if there
        // are some errors while fetching the list of the drivers
        subscription.add(
                locationObservable.subscribeOn(mSchedulers.main())
                        .observeOn(mSchedulers.main())
                        .subscribe(this::onLocationUpdated,
                                e -> {
                                    mView.notifyError(R.string.failed_to_get_drivers_informations);
                                }));

        // only in case of new run the map is centered with the user
        if (mView.isNewRun()) {
            subscription.add(
                    locationObservable.take(1).subscribeOn(mSchedulers.main())
                            .observeOn(mSchedulers.main())
                            .subscribe(this::centerView,
                                    e -> {
                                    }));
        }
    }

    @Override
    public void onViewAttached(DriversView view) {
        mSubscription = new CompositeSubscription();
        mView = view;
        mView.askForLocationPermission()
                .subscribeOn(mSchedulers.main())
                .observeOn(mSchedulers.main())
                .subscribe(permissionGranted -> {
                    startSubscriptions(permissionGranted, mSubscription);
                    if (permissionGranted) {
                        mView.enableUserLocation();
                    }
                });

        if (mNearestDriver == null) {
            mView.hideNearestDriverInfo();
        }
    }

    @Override
    public void onViewDetached() {
        if (mSubscription != null) {
            mSubscription.unsubscribe();
        }
    }

    @Override
    public void onCenterNearestDriverPressed() {
        if (mNearestDriver == null) {
            mView.notifyError(R.string.nearest_driver_not_found_yet);
            return;
        }
        if (mCurrentLocation == null) {
            mView.notifyError(R.string.current_location_not_yet_found);
            return;
        }

        double south = Math.min(mCurrentLocation.getLatitude(),
                mNearestDriver.getCoordinates().getLatitude());
        double north = Math.max(mCurrentLocation.getLatitude(),
                mNearestDriver.getCoordinates().getLatitude());

        double west = Math.min(mCurrentLocation.getLongitude(),
                mNearestDriver.getCoordinates().getLongitude());
        double east = Math.max(mCurrentLocation.getLongitude(),
                mNearestDriver.getCoordinates().getLongitude());

        Coordinates southWest = new Coordinates(south, west);
        Coordinates northEast = new Coordinates(north, east);

        mView.zoomToNearest(southWest, northEast);
    }
}
