package com.whiterabbit.wheresmydriver.screens.main;


import com.whiterabbit.wheresmydriver.inject.ActivityScope;
import com.whiterabbit.wheresmydriver.inject.ApplicationComponent;

import dagger.Component;

@ActivityScope
@Component(modules = {DriversModule.class},
        dependencies = {ApplicationComponent.class})
public interface DriversComponent {
    void inject(DriversActivity f);
}
