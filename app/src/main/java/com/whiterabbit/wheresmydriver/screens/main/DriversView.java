package com.whiterabbit.wheresmydriver.screens.main;


import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.model.Driver;

import java.util.List;

import rx.Observable;

public interface DriversView {
    void updateDrivers(List<Driver> driver, Driver nearest);

    void zoomToNearest(Coordinates southwest, Coordinates northeast);

    void updateNearestDriverInfo(Driver nearest, long distance);

    Observable<Boolean> askForLocationPermission();

    Observable<Coordinates> getLocationUpdates();

    void notifyError(int messageId);

    void displayNearestDriverInfo();

    void enableUserLocation();

    void centerTo(Coordinates coordinates);

    boolean isNewRun();

    void hideNearestDriverInfo();
}
