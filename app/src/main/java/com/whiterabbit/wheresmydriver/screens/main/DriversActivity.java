package com.whiterabbit.wheresmydriver.screens.main;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;
import com.tbruyelle.rxpermissions.RxPermissions;
import com.whiterabbit.wheresmydriver.R;
import com.whiterabbit.wheresmydriver.WMDApplication;
import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.model.Driver;
import com.whiterabbit.wheresmydriver.utils.Constants;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;
import rx.Observable;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class DriversActivity extends AppCompatActivity implements DriversView, OnMapReadyCallback {
    @Inject
    DriversPresenter mPresenter;
    @Inject
    RxPermissions mPermissionProvider;
    @Inject
    ReactiveLocationProvider mLocationProvider;
    @Inject
    MapMarkerFactory mMarkerFactory;

    @Bind(R.id.driver_info)
    TextView mDriverInfo;

    @Bind(R.id.detail_container)
    FrameLayout mDetailContainer;

    @Bind(R.id.driver_distance)
    TextView mDriverDistance;

    @Bind(R.id.center_driver_button)
    FloatingActionButton mCenterButton;

    @Bind(R.id.user_avatar)
    ImageView mAvatar;

    SupportMapFragment mMapFragment;
    GoogleMap mMap;
    boolean mIsNewRun;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        WMDApplication app = (WMDApplication) getApplication();
        DaggerDriversComponent.builder()
                .applicationComponent(app.getComponent())
                .driversModule(new DriversModule(this))
                .build().inject(this);

        ButterKnife.bind(this);
        mMapFragment =
                (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.drivers_map);

        mIsNewRun = (savedInstanceState == null);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // by doing this I attach the view only when I am sure the map can be used.
        // The first time the presenter is attached when the map is ready. After pause / resume the
        // presenter is attached immediately
        if (mMap == null) {
            mMapFragment.getMapAsync(this);
        } else {
            mPresenter.onViewAttached(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPresenter.onViewDetached();
    }

    @Override
    public void updateDrivers(List<Driver> drivers, Driver nearest) {
        mMap.clear();
        for (Driver d : drivers) {
            addMarker(d, d.equals(nearest));
        }
    }

    @Override
    public void zoomToNearest(Coordinates southwest, Coordinates northeast) {
        LatLngBounds bounds = new LatLngBounds(new LatLng(southwest.getLatitude(),
                southwest.getLongitude()),
                new LatLng(northeast.getLatitude(),
                        northeast.getLongitude()));

        int cameraPadding = (int) getResources().getDimension(R.dimen.margin_xlarge);
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, cameraPadding);
        mMap.animateCamera(cu);
    }

    @Override
    public void updateNearestDriverInfo(Driver nearest, long distance) {
        mDriverInfo.setText(String.format(Locale.getDefault(),
                "%s %s", nearest.getFirstname(), nearest.getLastname()));
        mDriverDistance.setText(String.format(Locale.getDefault(), "%d km", distance));

        Picasso.with(this).load(Constants.BASE_URL_IMG + nearest.getImage()).fit().into(mAvatar);
    }

    @Override
    public Observable<Boolean> askForLocationPermission() {
        return mPermissionProvider.request(Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);
    }

    @Override
    public Observable<Coordinates> getLocationUpdates() {
        LocationRequest request = LocationRequest.create() //standard GMS LocationRequest
                .setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY)
                .setInterval(1000);
        return mLocationProvider.getUpdatedLocation(request).map(l ->
                new Coordinates(l.getLatitude(), l.getLongitude()));
    }

    @Override
    public void notifyError(int message) {
        Toast.makeText(this, getString(message), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideNearestDriverInfo() {
        mDetailContainer.setVisibility(GONE);
        mCenterButton.setVisibility(GONE);
    }

    @Override
    public void displayNearestDriverInfo() {
        mDetailContainer.setVisibility(VISIBLE);
        mCenterButton.setVisibility(VISIBLE);
    }

    @Override
    public void enableUserLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
    }

    @Override
    public void centerTo(Coordinates coordinates) {
        CameraUpdate center =
                CameraUpdateFactory.newLatLng(new LatLng(coordinates.getLatitude(),
                        coordinates.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(10);

        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }

    @Override
    public boolean isNewRun() {
        return mIsNewRun;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mPresenter.onViewAttached(this);
    }

    @OnClick(R.id.center_driver_button)
    public void onCenterPressed() {
        mPresenter.onCenterNearestDriverPressed();
    }

    private void addMarker(Driver driver, boolean isNearest) {
        Bitmap b = isNearest ? mMarkerFactory.getNearestMapMarker(this) :
                mMarkerFactory.getNormalMapMarker(this);

        LatLng pos = new LatLng(driver.getCoordinates().getLatitude(),
                driver.getCoordinates().getLongitude());

        BitmapDescriptor icon = BitmapDescriptorFactory.fromBitmap(b);
        mMap.addMarker(new MarkerOptions().position(pos).icon(icon));
    }
}
