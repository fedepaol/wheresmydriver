package com.whiterabbit.wheresmydriver.screens.main;


import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;
import com.whiterabbit.wheresmydriver.utils.DistanceCalculator;

import dagger.Module;
import dagger.Provides;

@Module
public class DriversModule {
    private DriversView mView;

    public DriversModule(DriversView view) {
        mView = view;
    }

    @Provides
    public DriversView provideMainView() {
        return mView;
    }

    @Provides
    public DriversPresenter provideDriversPresenter(SchedulersProvider schedulersProvider,
                                                    DriversRestClient client,
                                                    DistanceCalculator calculator) {
        return new DriversPresenterImpl(schedulersProvider,
                client,
                calculator);
    }

}
