package com.whiterabbit.wheresmydriver.screens.main;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.LruCache;

import com.whiterabbit.wheresmydriver.R;


public class MapMarkerFactory {
    private LruCache<String, Bitmap> mMemoryCache;
    private Context mContext;

    public MapMarkerFactory(Context context) {
        mContext = context;
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {

                return bitmap.getByteCount() / 1024;
            }
        };
    }

    private Bitmap getMapMarker(Context c, boolean isNearest) {
        String cacheKey = String.valueOf(isNearest);

        Bitmap res = mMemoryCache.get(cacheKey);
        if (res != null) {
            return res;
        }

        Drawable vectorDrawable = isNearest ?
                ContextCompat.getDrawable(mContext, R.drawable.ic_local_driver_nearest) :
                ContextCompat.getDrawable(mContext, R.drawable.ic_local_driver);

        res = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(),
                vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(res);
        vectorDrawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        vectorDrawable.draw(canvas);

        mMemoryCache.put(cacheKey, res);
        return res;
    }

    public Bitmap getNearestMapMarker(Context c) {
        return getMapMarker(c, true);
    }

    public Bitmap getNormalMapMarker(Context c) {
        return getMapMarker(c, false);
    }

}
