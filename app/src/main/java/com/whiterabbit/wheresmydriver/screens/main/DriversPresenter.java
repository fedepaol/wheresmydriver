package com.whiterabbit.wheresmydriver.screens.main;


public interface DriversPresenter {
    void onViewAttached(DriversView view);

    void onViewDetached();

    void onCenterNearestDriverPressed();
}
