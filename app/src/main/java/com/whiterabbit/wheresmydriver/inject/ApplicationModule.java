package com.whiterabbit.wheresmydriver.inject;

import android.app.Application;
import android.content.Context;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.schedule.RealSchedulersProvider;
import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;
import com.whiterabbit.wheresmydriver.screens.main.MapMarkerFactory;
import com.whiterabbit.wheresmydriver.utils.DistanceCalculator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;


@Module
public class ApplicationModule {
    private Application mApp;

    public ApplicationModule(Application app) {
        mApp = app;
    }

    @Provides
    @Singleton
    ReactiveLocationProvider provideLocation() {
        return new ReactiveLocationProvider(mApp.getApplicationContext());
    }

    @Provides
    @Singleton
    Context provideContext() {
        return mApp.getApplicationContext();
    }

    @Provides
    @Singleton
    SchedulersProvider provideSchedulers() {
        return new RealSchedulersProvider();
    }

    @Provides
    @Singleton
    RxPermissions provideRxPermissions(Context c) {
        return RxPermissions.getInstance(c);
    }

    @Provides
    @Singleton
    DriversRestClient provideRestClient() {
        return new DriversRestClient();
    }

    @Provides
    @Singleton
    DistanceCalculator provideDistanceCalculator() {
        return new DistanceCalculator();
    }

    @Provides
    @Singleton
    public MapMarkerFactory provideMapMarkerFactory(Context c) {
        return new MapMarkerFactory(c);
    }
}
