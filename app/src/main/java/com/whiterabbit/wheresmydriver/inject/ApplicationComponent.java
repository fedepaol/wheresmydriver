package com.whiterabbit.wheresmydriver.inject;


import android.content.Context;

import com.tbruyelle.rxpermissions.RxPermissions;
import com.whiterabbit.wheresmydriver.WMDApplication;
import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;
import com.whiterabbit.wheresmydriver.screens.main.MapMarkerFactory;
import com.whiterabbit.wheresmydriver.utils.DistanceCalculator;

import javax.inject.Singleton;

import dagger.Component;
import pl.charmas.android.reactivelocation.ReactiveLocationProvider;

@Singleton
@Component(modules = {ApplicationModule.class})
public interface ApplicationComponent {
    void inject(WMDApplication app);

    ReactiveLocationProvider getReactiveLocationProvider();

    Context getContext();

    SchedulersProvider getSchedulers();

    RxPermissions getPermissions();

    DriversRestClient getRestClient();

    DistanceCalculator getDistCalculator();

    MapMarkerFactory getMarkerFactory();
}


