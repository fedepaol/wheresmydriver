package com.whiterabbit.wheresmydriver.restclient;


import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.model.Driver;

import java.util.List;

import retrofit2.http.Body;
import retrofit2.http.PUT;
import rx.Observable;

public interface DriversService {
    @PUT("coordinates")
    Observable<List<Driver>> listDrivers(@Body Coordinates myLocation);
}
