
package com.whiterabbit.wheresmydriver.restclient;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.model.Driver;
import com.whiterabbit.wheresmydriver.utils.Constants;

import java.util.List;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class DriversRestClient {

    private DriversService mClient;

    private Gson getGSon() {
        GsonBuilder builder = new GsonBuilder();
        return builder.create();
    }

    public DriversRestClient() {
        OkHttpClient okClient = new OkHttpClient.Builder().build();

        mClient = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(okClient)
                .addConverterFactory(GsonConverterFactory.create(getGSon()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build()
                .create(DriversService.class);
    }

    public Observable<List<Driver>> getDrivers(Coordinates c) {
        return mClient.listDrivers(c);
    }

}
