
package com.whiterabbit.wheresmydriver.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Driver {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("coordinates")
    @Expose
    private Coordinates coordinates;

    /**
     * @return The id
     */
    public int getId() {
        return id;
    }

    /**
     * @return The firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * @return The lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * @return The image
     */
    public String getImage() {
        return image;
    }

    /**
     * @return The coordinates
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * @param coordinates The coordinates
     */
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Driver)) return false;
        final Driver that = (Driver) obj;
        return this.getId() == that.getId();
    }

    @Override
    public int hashCode() {
        return id;
    }
}
