package com.whiterabbit.wheresmydriver;


import android.app.Application;

import com.whiterabbit.wheresmydriver.inject.ApplicationComponent;
import com.whiterabbit.wheresmydriver.inject.ApplicationModule;
import com.whiterabbit.wheresmydriver.inject.DaggerApplicationComponent;

public class WMDApplication extends Application {
    private ApplicationComponent mComponent;

    ApplicationModule getApplicationModule() {
        return new ApplicationModule(this);
    }

    void initComponent() {
        mComponent = DaggerApplicationComponent.builder()
                .applicationModule(getApplicationModule())
                .build();
    }

    public ApplicationComponent getComponent() {
        if (mComponent == null) {
            initComponent();
        }

        return mComponent;
    }

}
