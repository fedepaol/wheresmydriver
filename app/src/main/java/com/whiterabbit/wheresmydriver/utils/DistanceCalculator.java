package com.whiterabbit.wheresmydriver.utils;


import android.location.Location;

import com.whiterabbit.wheresmydriver.model.Coordinates;

// Could have used Location.distanceBetween, but it would not have been possible to test
// it inside unit tests
public class DistanceCalculator {
    public float getDistance(Coordinates p1, Coordinates p2) {
        float[] res = {};
        Location.distanceBetween(p1.getLatitude(),
                p1.getLongitude(),
                p2.getLatitude(),
                p2.getLongitude(), res);

        return res[0] / 1000;
    }

    public long distance(Coordinates p1, Coordinates p2) {
        double earthRadius = 6371; //Kmeters
        double dLat = Math.toRadians(p2.getLatitude() - p1.getLatitude());
        double dLng = Math.toRadians(p2.getLongitude() - p1.getLongitude());
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(p1.getLatitude())) * Math.cos(Math.toRadians(p2.getLatitude())) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (long) (earthRadius * c);
    }

}
