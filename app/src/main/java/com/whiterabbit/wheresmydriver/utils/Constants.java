package com.whiterabbit.wheresmydriver.utils;


import com.whiterabbit.wheresmydriver.model.Coordinates;

public class Constants {
    public final static double PARIS_LATITUDE = 48.858794;
    public final static double PARIS_LONGITUDE = 2.293816;
    public final static String BASE_URL = "http://baseurl.xxx.com/mobile/";
    public final static String BASE_URL_IMG = "http://images.xxx.com";

    public final static Coordinates PARIS_COORDINATES = new Coordinates(PARIS_LATITUDE,
            PARIS_LONGITUDE);

    private Constants() {

    }


}
