package com.whiterabbit.wheresmydriver;


import com.jakewharton.rxrelay.BehaviorRelay;
import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.model.Driver;
import com.whiterabbit.wheresmydriver.restclient.DriversRestClient;
import com.whiterabbit.wheresmydriver.screens.main.DriversPresenterImpl;
import com.whiterabbit.wheresmydriver.screens.main.DriversView;
import com.whiterabbit.wheresmydriver.utils.Constants;
import com.whiterabbit.wheresmydriver.utils.DistanceCalculator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import mockdata.FakeSchedulersProvider;
import mockdata.TestDataProvider;
import rx.Observable;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DriversPresenterTest {
    private FakeSchedulersProvider mScheduler;
    private DriversPresenterImpl mPresenter;

    @Mock
    private DriversRestClient mRestClient;
    @Mock
    private DriversView mView;
    @Captor
    private ArgumentCaptor<List<Driver>> driversCaptor;

    private List<Driver> mPisaLuccaDriverList;
    private List<Driver> mLivornoFirenzeDriverList;


    @Before
    public void setup() {
        TestDataProvider dataProvider = new TestDataProvider();
        mPisaLuccaDriverList = dataProvider.getPisaLuccaDrivers();
        mLivornoFirenzeDriverList = dataProvider.getLivornoFirenzeDrivers();

        when(mRestClient.getDrivers(any(Coordinates.class)))
                .thenReturn(Observable.just(mPisaLuccaDriverList))
                .thenReturn(Observable.just(mLivornoFirenzeDriverList))
                .thenReturn(Observable.just(mPisaLuccaDriverList));

        mScheduler = new FakeSchedulersProvider();
        mPresenter = new DriversPresenterImpl(mScheduler,
                mRestClient,
                new DistanceCalculator());
        when(mView.isNewRun()).thenReturn(true);
    }

    private void verifyNearestDriver(List<Driver> drivers,
                                     List<Long> distances,
                                     int index,
                                     String nameToVerify,
                                     long distanceToVerify) {
        Driver d = drivers.get(index);
        long driverDistance = distances.get(index);
        assertEquals(d.getFirstname(), nameToVerify);
        assertEquals(driverDistance, distanceToVerify);
    }

    private void verifyNearestDriver(List<Driver> drivers,
                                     int index,
                                     String nameToVerify) {
        Driver d = drivers.get(index);
        assertEquals(d.getFirstname(), nameToVerify);
    }

    /**
     * Verifies the name of the first record of the index-th list
     * and the size of the list
     *
     * @param drivers
     * @param index
     * @param size
     * @param nameToVerify
     */
    private void verifyDataDriverFromList(List<List<Driver>> drivers,
                                          int index,
                                          int size,
                                          String nameToVerify) {
        Driver d = drivers.get(index).get(0);
        assertEquals(d.getFirstname(), nameToVerify);
        assertEquals(drivers.get(index).size(), size);
    }

    @Test
    public void testNoPermission() {
        // no permissions: stuck on paris
        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(false));

        mPresenter.onViewAttached(mView);
        mScheduler.getTestScheduler().advanceTimeBy(10, TimeUnit.SECONDS);

        mPresenter.onViewDetached();
        mScheduler.getTestScheduler().advanceTimeBy(200, TimeUnit.SECONDS);

        verify(mView, never()).enableUserLocation();

        ArgumentCaptor<Driver> driverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView, times(3)).updateDrivers(driversCaptor.capture(), driverCaptor.capture());

        List<List<Driver>> driversValues = driversCaptor.getAllValues();

        verifyDataDriverFromList(driversValues, 0, 2, "Pisa");
        verifyDataDriverFromList(driversValues, 1, 2, "Livorno");
        verifyDataDriverFromList(driversValues, 2, 2, "Pisa");

        ArgumentCaptor<Driver> nearestDriverCaptor = ArgumentCaptor.forClass(Driver.class);
        ArgumentCaptor<Long> distanceCaptor = ArgumentCaptor.forClass(Long.class);

        verify(mView, times(3)).updateNearestDriverInfo(nearestDriverCaptor.capture(),
                distanceCaptor.capture());
        List<Driver> nearestDrivers = nearestDriverCaptor.getAllValues();

        List<Long> distances = distanceCaptor.getAllValues();
        verifyNearestDriver(nearestDrivers, distances, 0, "Lucca", 840);
        verifyNearestDriver(nearestDrivers, distances, 1, "Livorno", 853);
        verifyNearestDriver(nearestDrivers, distances, 2, "Lucca", 840);

    }

    @Test
    public void testPermission() {
        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(true));

        // A location observable that pretends to emit immediately, after 2 seconds and after 6
        // seconds.
        Coordinates pisaCoordinates = mPisaLuccaDriverList.get(0).getCoordinates();
        Coordinates livornoCoordinates = mLivornoFirenzeDriverList.get(0).getCoordinates();
        Coordinates firenzeCoordinates = mLivornoFirenzeDriverList.get(1).getCoordinates();

        Observable<Coordinates> res = Observable.just(pisaCoordinates).delay(1, TimeUnit.SECONDS,
                mScheduler.computation())
                .concatWith(Observable.just(livornoCoordinates)
                        .delay(1, TimeUnit.SECONDS,
                                mScheduler.computation()))
                .concatWith(Observable.just(firenzeCoordinates)
                        .delay(3, TimeUnit.SECONDS,
                                mScheduler.computation()));

        when(mView.getLocationUpdates()).thenReturn(res);

        mPresenter.onViewAttached(mView);
        mScheduler.getTestScheduler().advanceTimeBy(7, TimeUnit.SECONDS);

        mPresenter.onViewDetached();
        mScheduler.getTestScheduler().advanceTimeBy(200, TimeUnit.SECONDS);


        verify(mView).enableUserLocation();

        ArgumentCaptor<Driver> driverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView, times(2)).updateDrivers(driversCaptor.capture(), driverCaptor.capture());

        // I expect the updatedrivers to be called exactly 2 times, one initially and one after 5 seconds

        List<List<Driver>> driversValues = driversCaptor.getAllValues();
        verifyDataDriverFromList(driversValues, 0, 2, "Pisa");
        verifyDataDriverFromList(driversValues, 1, 2, "Livorno");

        // The location moves between Pisa, livorno, firenze
        // After the second location update the data is restored to pisa / lucca
        ArgumentCaptor<Driver> nearestDriverCaptor = ArgumentCaptor.forClass(Driver.class);
        ArgumentCaptor<Long> distanceCaptor = ArgumentCaptor.forClass(Long.class);

        verify(mView, times(4)).updateNearestDriverInfo(nearestDriverCaptor.capture(),
                distanceCaptor.capture());
        List<Driver> nearestDrivers = nearestDriverCaptor.getAllValues();
        List<Long> distances = distanceCaptor.getAllValues();
        verifyNearestDriver(nearestDrivers, distances, 0, "Pisa", 0);
        verifyNearestDriver(nearestDrivers, distances, 1, "Pisa", 20);
        verifyNearestDriver(nearestDrivers, distances, 2, "Livorno", 0);
        verifyNearestDriver(nearestDrivers, distances, 3, "Firenze", 0);

        ArgumentCaptor<Coordinates> coordinatesCaptor = ArgumentCaptor.forClass(Coordinates.class);
        verify(mView).centerTo(coordinatesCaptor.capture());
        assertEquals(coordinatesCaptor.getValue().getLatitude(), pisaCoordinates.getLatitude(), 0.01);
        assertEquals(coordinatesCaptor.getValue().getLongitude(), pisaCoordinates.getLongitude(), 0.01);
    }

    @Test
    public void testPermissionSlowFirstLocation() {
        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(true));

        // A location observable that pretends to emit immediately, after 2 seconds and after 6
        // seconds.
        // THIS drove me mad. The delay started after the timeout so it was 7 seconds in total.
        // By using a publish relay I can simulate a source that emits EXACTLY after 4 seconds from now
        Coordinates pisaCoordinates = mPisaLuccaDriverList.get(0).getCoordinates();
        BehaviorRelay<Coordinates> res = BehaviorRelay.create();
        Observable.just(pisaCoordinates)
                .delay(4, TimeUnit.SECONDS, mScheduler.computation()).subscribe(res);


        when(mView.getLocationUpdates()).thenReturn(res);

        mPresenter.onViewAttached(mView);
        verify(mView).hideNearestDriverInfo(); // the first time the info are not visible
        mScheduler.getTestScheduler().advanceTimeBy(7, TimeUnit.SECONDS);
        verify(mView).displayNearestDriverInfo();
        mPresenter.onViewDetached();
        mScheduler.getTestScheduler().advanceTimeBy(200, TimeUnit.SECONDS);


        verify(mView).enableUserLocation();

        ArgumentCaptor<Driver> driverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView, times(2)).updateDrivers(driversCaptor.capture(), driverCaptor.capture());

        // I expect the updatedrivers to be called exactly 2 times, one initially and one after 5 seconds

        List<List<Driver>> driversValues = driversCaptor.getAllValues();
        verifyDataDriverFromList(driversValues, 0, 2, "Pisa");
        verifyDataDriverFromList(driversValues, 1, 2, "Livorno");


        // The location starts from Paris and goes to Pisa after 3.5 seconds
        ArgumentCaptor<Driver> nearestDriverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView, times(3)).updateNearestDriverInfo(nearestDriverCaptor.capture(),
                any(Long.class));
        List<Driver> nearestDrivers = nearestDriverCaptor.getAllValues();

        verifyNearestDriver(nearestDrivers, 0, "Lucca");
        verifyNearestDriver(nearestDrivers, 1, "Pisa");
        verifyNearestDriver(nearestDrivers, 2, "Livorno");

        ArgumentCaptor<Coordinates> coordinatesCaptor = ArgumentCaptor.forClass(Coordinates.class);
        verify(mView).centerTo(coordinatesCaptor.capture());
        assertEquals(coordinatesCaptor.getValue().getLatitude(), Constants.PARIS_LATITUDE, 0.01);
        assertEquals(coordinatesCaptor.getValue().getLongitude(), Constants.PARIS_LONGITUDE, 0.01);

    }

    @Test
    public void testCentersOnlyFirstTime() {
        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(true));

        // The location emits only the first time, then stops
        // The list of the drivers gets updated 2 times anyway
        Coordinates pisaCoordinates = mPisaLuccaDriverList.get(0).getCoordinates();

        Observable<Coordinates> res = Observable.just(pisaCoordinates);
        when(mView.getLocationUpdates()).thenReturn(res);
        when(mView.isNewRun()).thenReturn(true);
        mPresenter.onViewAttached(mView);
        mPresenter.onViewDetached();
        when(mView.isNewRun()).thenReturn(false);
        mPresenter.onViewAttached(mView);
        verify(mView).centerTo(any(Coordinates.class)); // only once
    }

    @Test
    public void testPermissionLocationStops() {
        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(true));

        // The location emits only the first time, then stops
        // The list of the drivers gets updated 2 times anyway
        Coordinates pisaCoordinates = mPisaLuccaDriverList.get(0).getCoordinates();

        Observable<Coordinates> res = Observable.just(pisaCoordinates);
        when(mView.getLocationUpdates()).thenReturn(res);

        mPresenter.onViewAttached(mView);
        mScheduler.getTestScheduler().advanceTimeBy(7, TimeUnit.SECONDS);
        mPresenter.onViewDetached();
        mScheduler.getTestScheduler().advanceTimeBy(200, TimeUnit.SECONDS);


        ArgumentCaptor<Driver> driverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView, times(2)).updateDrivers(driversCaptor.capture(), driverCaptor.capture());

        List<List<Driver>> driversValues = driversCaptor.getAllValues();
        verifyDataDriverFromList(driversValues, 0, 2, "Pisa");
        verifyDataDriverFromList(driversValues, 1, 2, "Livorno");

        // The nearest is updated twice, depending only on the result of the rest call
        ArgumentCaptor<Driver> nearestDriverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView, times(2)).updateNearestDriverInfo(nearestDriverCaptor.capture(), any(Long.class));
        List<Driver> nearestDrivers = nearestDriverCaptor.getAllValues();
        verifyNearestDriver(nearestDrivers, 0, "Pisa");
        verifyNearestDriver(nearestDrivers, 1, "Livorno");

    }

    @Test
    public void testFetchError() {
        // The first call fails, it retries after 5 seconds
        when(mRestClient.getDrivers(any(Coordinates.class)))
                .thenReturn(Observable.error(new IOException()))
                .thenReturn(Observable.just(mPisaLuccaDriverList));

        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(true));

        Coordinates pisaCoordinates = mPisaLuccaDriverList.get(0).getCoordinates();
        Observable<Coordinates> res = Observable.just(pisaCoordinates);
        when(mView.getLocationUpdates()).thenReturn(res);

        mPresenter.onViewAttached(mView);
        mScheduler.getTestScheduler().advanceTimeBy(7, TimeUnit.SECONDS);
        mPresenter.onViewDetached();
        mScheduler.getTestScheduler().advanceTimeBy(200, TimeUnit.SECONDS);

        verify(mView).notifyError(R.string.failed_to_get_drivers_informations);
        ArgumentCaptor<Driver> nearestDriverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView).updateNearestDriverInfo(nearestDriverCaptor.capture(), any(Long.class));
        List<Driver> nearestDrivers = nearestDriverCaptor.getAllValues();
        verifyNearestDriver(nearestDrivers, 0, "Pisa");
    }

    @Test
    public void testButtonPressed() {
        when(mView.askForLocationPermission())
                .thenReturn(Observable.just(true));

        // A location observable that pretends to emit immediately, after 2 seconds and after 6
        // seconds.
        Coordinates livornoCoordinates = mLivornoFirenzeDriverList.get(0).getCoordinates();
        Coordinates pisaCoordinates = mPisaLuccaDriverList.get(0).getCoordinates();


        Observable<Coordinates> res = Observable.just(livornoCoordinates);
        when(mView.getLocationUpdates()).thenReturn(res);
        mPresenter.onViewAttached(mView);

        ArgumentCaptor<Driver> nearestDriverCaptor = ArgumentCaptor.forClass(Driver.class);
        verify(mView).updateNearestDriverInfo(nearestDriverCaptor.capture(), any(Long.class));
        Driver nearestDriver = nearestDriverCaptor.getValue();

        assertEquals(nearestDriver.getFirstname(), "Pisa");
        // current location is livorno, nearest driver pisa

        mPresenter.onCenterNearestDriverPressed();

        ArgumentCaptor<Coordinates> southWestCaptor = ArgumentCaptor.forClass(Coordinates.class);
        ArgumentCaptor<Coordinates> northEastCaptor = ArgumentCaptor.forClass(Coordinates.class);
        verify(mView).zoomToNearest(southWestCaptor.capture(), northEastCaptor.capture());

        Coordinates southWest = southWestCaptor.getValue();
        Coordinates northEast = northEastCaptor.getValue();

        assertEquals(southWest.getLatitude(), livornoCoordinates.getLatitude(), 0.001);
        assertEquals(southWest.getLongitude(), livornoCoordinates.getLongitude(), 0.001);

        assertEquals(northEast.getLatitude(), pisaCoordinates.getLatitude(), 0.001);
        assertEquals(northEast.getLongitude(), pisaCoordinates.getLongitude(), 0.001);

    }
}
