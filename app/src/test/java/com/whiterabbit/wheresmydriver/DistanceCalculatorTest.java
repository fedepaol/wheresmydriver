package com.whiterabbit.wheresmydriver;


import com.whiterabbit.wheresmydriver.model.Coordinates;
import com.whiterabbit.wheresmydriver.utils.DistanceCalculator;

import org.junit.Test;

import static junit.framework.TestCase.assertTrue;

public class DistanceCalculatorTest {
    private DistanceCalculator mCalculator;

    public DistanceCalculatorTest() {
        mCalculator = new DistanceCalculator();
    }

    @Test
    public void pisaFirenze() {
        Coordinates pisa = new Coordinates(43.722805, 10.396615);
        Coordinates firenze = new Coordinates(43.767701, 11.254018);
        double distance = mCalculator.distance(pisa, firenze);
        assertTrue(distance > 68 && distance < 70); // 69 kms
    }
}
