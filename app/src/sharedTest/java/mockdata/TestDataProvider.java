package mockdata;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.whiterabbit.wheresmydriver.model.Driver;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class TestDataProvider {
    private final String JSON_SAMPLE = "[\n" +
            "  {\n" +
            "    \"id\": 1,\n" +
            "    \"firstname\": \"Pisa\",\n" +
            "    \"lastname\": \"Delapique\",\n" +
            "    \"image\": \"/mobile/images/nutty.jpg\",\n" +
            "    \"coordinates\": {\n" +
            "      \"latitude\": 43.722805,\n" +
            "      \"longitude\": 10.396615\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 2,\n" +
            "    \"firstname\": \"Lucca\",\n" +
            "    \"lastname\": \"Tupperware\",\n" +
            "    \"image\": \"/mobile/images/cuddles.jpg\",\n" +
            "    \"coordinates\": {\n" +
            "      \"latitude\": 43.843665,\n" +
            "      \"longitude\": 10.504073\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 3,\n" +
            "    \"firstname\": \"Livorno\",\n" +
            "    \"lastname\": \"Faillence\",\n" +
            "    \"image\": \"/mobile/images/flippy.jpg\",\n" +
            "    \"coordinates\": {\n" +
            "      \"latitude\": 43.545501,\n" +
            "      \"longitude\":  10.318135\n" +
            "    }\n" +
            "  },\n" +
            "  {\n" +
            "    \"id\": 4,\n" +
            "    \"firstname\": \"Firenze\",\n" +
            "    \"lastname\": \"Fanastick\",\n" +
            "    \"image\": \"/mobile/images/flaky.jpg\",\n" +
            "    \"coordinates\": {\n" +
            "      \"latitude\": 43.767701,\n" +
            "      \"longitude\": 11.254018\n" +
            "    }\n" +
            "  }\n" +
            "]";

    private List<Driver> mDrivers;
    private List<Driver> mDrivers1;
    private List<Driver> mAllDrivers;

    public TestDataProvider() {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Type listType = new TypeToken<ArrayList<Driver>>(){}.getType();
        mAllDrivers = gson.fromJson(JSON_SAMPLE, listType);
        mDrivers = mAllDrivers.subList(0, 2);
        mDrivers1 = mAllDrivers.subList(2, 4);
    }

    public List<Driver> getPisaLuccaDrivers() {
        return mDrivers;
    }

    public List<Driver> getLivornoFirenzeDrivers() {
        return mDrivers1;
    }

    public List<Driver> getAllDrivers() { return mAllDrivers; }

    public Driver getDriverByName(String name) {
        for (Driver d : mAllDrivers) {
            if (d.getFirstname().equals(name))
                return d;
        }
        return null;
    }
}
