package mockdata;


import com.whiterabbit.wheresmydriver.schedule.SchedulersProvider;

import rx.Scheduler;
import rx.schedulers.Schedulers;
import rx.schedulers.TestScheduler;

public class FakeSchedulersProvider implements SchedulersProvider {
    private TestScheduler testScheduler;

    public FakeSchedulersProvider() {
        testScheduler = new TestScheduler();
    }

    @Override
    public Scheduler main() {
        return Schedulers.immediate();
    }

    @Override
    public Scheduler io() {
        return Schedulers.immediate();
    }

    @Override
    public Scheduler computation() {
        return testScheduler;
    }

    public TestScheduler getTestScheduler() {
        return testScheduler;
    }
}
