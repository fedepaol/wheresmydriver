package mockdata;


import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

public class EspressoTestSchedulersProvider extends FakeSchedulersProvider {
    @Override
    public Scheduler main() {
        return AndroidSchedulers.mainThread();
    }
}
