##A few notes about the app design:

### Architecture
It uses the architecture I am most familiar with: plain MVP plus Dagger 2.

I used a single activity in favour of a Fragment because otherwise I would have had to choose the lesser evil between Fragment in Fragment vs MapView (which needs to propagate all the lifecycle event to the mapview) .

This is probably the first time I managed to have a "vanilla java" presenter, with no dependencies from Android at all.

I recently published a similar app on the store which helped a bit for the view part, but here the challenge was more about keeping in synch the fact that both the user's location AND the drivers could move.
### RxJava
This is certainly the most complex rxjava example I put in place.
Part of the reason is to show off what I know (or don't know) about handling reactive streams. 

I wanted to handle in a single chain those cases when the location is not updated but the list of drivers need to be fetched anyway.
The same result would have been easier (and maybe more readable) by having the rest client to be polled by a interval stream and use the updated current location variable (with Observable.fromCallable()), instead of using the "unreliable" location stream as originating source and caching the last emitted result. 

I had to inject the schedulers in order to be able to mock them with tests. Given timing is relevant, I used TestScheduler intensively (for the first time :) . From what I read, it's much easier to replace the schedulers with RxJava2, but I preferred to stick on what I know.

### Tests
I managed to test the presenter extensively. I had to understand how TestScheduler work but it helped a lot in order to test all the various combinations. 
I placed an UI Test using espresso, but that was limited to the details of the driver, since I did not found a way to test mapviews behaviours.

### GUI / UX:
The app looks decent on my phone (nexus 5x). However, I did not try lower resolutions / sizes to check if text is cut / etc.

The map is centered around the location of the user the first time the app is opened. This is optimal in a real world scenario, a little worse if the user is far away from the drivers.

I came up with the solution of not showing the nearest driver detail AT ALL as long as no data is available. This looks a lot better than showing an empty text view. UX could be improved with an indication of loading.

Map handling could be better. Since the list of the drivers is related to the location of the user, I assumed that the rest endpoint returns a limited number of markers.
Every time the map is updated, the markers are cleaned and recreated, no clustering of markers is involved, nor a smarter maintenance (like checking if the Driver is already displayed, and changing just it's icon if needed). 

The "used did not grand permission" path has been left behind. A good practice would be to try to educate the user about the need of that specific permission (even if it should sound obvious) and ask again. Here is an absolute yes or no. In case of No, the app assumes to be in the center of Paris and shows the nearest driver from that location. Another variant could have been to only show the drivers without highlighting the nearest one.

I used the free material icons + Roman Nurik's android icon generator. It's way better anything I could have drawn by hand :-)

### Conclusion
I liked setting up this app. 
There is space for improvement, but I think it covers all I was asked to do and provides a good understanding of what I know (and don't know) about Android. 



